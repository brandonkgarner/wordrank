/*
 * <h1>Count words, rank by use, and return to client.</h1>
 * Takes a text file supplied by client program, counts all the words
 * individually, ranks them by number of appearances in the document,
 * and returns to the client--the overall word count, individual word
 * counts, and word rankings.
 *
 * @author <a href="mailto:brandon.k.garner@gmail.com">Brandon Garner</a>
 * 
 */
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.stream.Collectors;

public class WordRankServer {
	public static void main(String[] args) throws Exception {
		// Check args for port
		if (args.length != 1) {
			System.out.println("Required arguments: port");
			return;
		}
		int port = Integer.parseInt(args[0]);
		while (true) { // keep Server running indefinitely
			try {
				// Open stream socket for accepting connections
				ServerSocket myConnectionSocket = new ServerSocket(port);
				System.out.println("\nServer ready.");
				System.out.print("Waiting for a connection... ");
				
				// Accept connection
				final Socket socket = myConnectionSocket.accept();

				// Create input/output streams to client
				DataInputStream input = new DataInputStream(socket.getInputStream());
				DataOutputStream output = new DataOutputStream(socket.getOutputStream());
				System.out.println("connection accepted.");

				// Read text file from client into String
				String message = input.readUTF();
				System.out.print("Document received, processing... ");

				// Remove punctuation
				String strippedMessage = message.replaceAll("[^\\s\\w-]", "");
				String listOfWords = ""; // to be returned to client
				
				// Remove case of words and insert to HashMap
				Map<String, Integer> words_count = new HashMap<String, Integer>();
				String[] words = strippedMessage.toLowerCase().split("\\s+");
				for (int i = 0; i < words.length; i++) {
					String s = words[i];
					
					// Determine if word is already in HashMap
					if (words_count.keySet().contains(s)) { // already a key
						Integer count = words_count.get(s) + 1;
						words_count.put(s, count);
					} else {
						words_count.put(s, 1); // new key
					}
				}
				int overallWordCount = words.length;
				// Arrange Map by values
				Map<String, Integer> sortedMap = sortByValue(words_count);
				Iterator<String> iterator = sortedMap.keySet().iterator();

				int listCounter = 0; // Used to rank words in ascending order
				String plural = "time";

				while (iterator.hasNext()) {
					String key = iterator.next().toString();
					String value = sortedMap.get(key).toString();
					++listCounter;
					// Change to plural if value is more than 1
					if (sortedMap.get(key) != 1)
						plural = "times";
					else
						plural = "time";
					// Fill string to be returned to client
					listOfWords += (listCounter + ": \"" + key + "\" Appeared " + value + " " + plural
							+ " in this document.\n");
				} // End while iteration for message
				System.out.print("done.\nSending results... ");

				// Send data back to client
				output.writeUTF(listOfWords + "Word count: " + overallWordCount);
				System.out.println("sent.");
				System.out.print("Document processed successfully, closing connection... ");

				myConnectionSocket.close(); // close connection server-side
				System.out.println("connection closed.");
			} catch (EOFException e) {
				// Do nothing, but don't print exception
			} catch (BindException e) {
				System.out.println("Using ports less than 1024 requires admin priviliges.");
				System.out.println("Please run as administrator or choose different port.");
				return;
			} catch (SocketException e) {
				// Do nothing, but don't print exception
			} catch (Exception ex) {
				ex.printStackTrace();
			} // End try/catch
		} // End forever loop
	} // End Main()

	// Allows for iterating Map to sort by values and return
	// http://stackoverflow.com/questions/109383/sort-a-mapkey-value-by-values-java
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	} // End sortByValue()
} // End Class
