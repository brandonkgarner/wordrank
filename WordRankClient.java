/*
 * <h1>Send text file to server and receive word count and ranking of words.</h1>
 * Sends a text file to a server program that counts all the words
 * individually, ranks them by number of appearances in the document,
 * and returns said data--the overall word count, individual word
 * counts, and word rankings.
 *
 * @author <a href="mailto:brandon.k.garner@gmail.com">Brandon Garner</a>
 * @version X
 * @since 2017-04-24
 * 
 * University:  NSU
 * Semester:    Winter, 2017
 * Class:       TECH4310
 * Project:     Final Project
 * File:        WordRankClient.java
 * 
 */
import java.io.*;
import java.net.*;

public class WordRankClient {
	public static void main(String[] args) throws Exception {
		// Get command line arguments.
		if (args.length != 3) {
			System.out.println("Required arguments: host port file.txt");
			return;
		}
		try {
			// Check for file first before contacting server
			FileInputStream fileIn = new FileInputStream(args[2]);

			// Set the server address and port
			InetAddress serverAddress = InetAddress.getByName(args[0]);
			int serverPort = Integer.parseInt(args[1]);

			// Create socket & input/output streams for server communication
			final Socket socket = new Socket(serverAddress, serverPort);
			DataInputStream input = new DataInputStream(socket.getInputStream());
			DataOutputStream output = new DataOutputStream(socket.getOutputStream());

			// Read text file and send to server
			byte[] buf = new byte[Short.MAX_VALUE];
			int bytesRead;
			while ((bytesRead = fileIn.read(buf)) != -1) {
				output.writeShort(bytesRead);
				output.write(buf, 0, bytesRead);
			}
			output.writeShort(-1);
			fileIn.close();

			// Wait for server to return message and then print to I/O
			System.out.println(input.readUTF());
			socket.close(); // Connection will be closed by server also
		} catch (FileNotFoundException e) {
			System.out.println("File \"" + (args[2]) + "\" cannot be found!");
			return;
		} catch (ConnectException e) {
			System.out.println("Could not connect to " + (args[0]) +":" +(args[1]));
			System.out.println("Please check your server address and port.");
			return;
		} catch (NoRouteToHostException e) {
			System.out.println("Server address is incorrect or server is down.");
			return;
		} catch (Exception ex) {
			ex.printStackTrace();
		} // End try/catch
	} // End main
} // End class
